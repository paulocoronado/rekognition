import boto3

def detectarEtiquetas(imagen):


    cliente=boto3.client('rekognition')
       
    with open(imagen, 'rb') as image:
        bytes_count=image.read()
        respuesta = cliente.detect_labels(Image={'Bytes': bytes_count})
        respuesta_caras = cliente.detect_faces(Image={'Bytes': bytes_count},Attributes=['ALL'])
    
    for face in respuesta_caras['FaceDetails']:
        print("Face ({Confidence}%)".format(**face))
        # emotions
        for emotion in face['Emotions']:
            print ("  {Type} : {Confidence}%".format(**emotion))
        
        
    print('Etiquetas detectadas en ' + imagen)    
    for label in respuesta['Labels']:
        print (label['Name'] + ' : ' + str(label['Confidence']))

    return len(respuesta['Labels'])

def main():
    miImagen='gente.jpg'
    totalEtiquetas=detectarEtiquetas(miImagen)
    print("Etiquetas Detectadas: " + str(totalEtiquetas))


if __name__ == "__main__":
    main()